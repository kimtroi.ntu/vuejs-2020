import Vue from 'vue'
import App from './App.vue'
// LightBootstrap plugin
import LightBootstrap from './light-bootstrap-main'
Vue.use(LightBootstrap)
// router
import router from './router'
// registerServiceWorker
import './registerServiceWorker'

Vue.config.productionTip = false

new Vue({
  router,
  render: h => h(App)
}).$mount('#app')
